require("functions")

-- give turrets some fire resistance
if settings.startup["issy-turret-fire-resistance"].value then
	local r = { type = "fire", percent = 75 }
	add_resistance("gun-turret", "ammo-turret", r)
	add_resistance("laser-turret", "electric-turret", r)

	if mods["ironclad-gunboat-and-mortar-turret"] then
		add_resistance("mortar-turret", "ammo-turret", r)
	end

	--[[ -- these already have 75% fire resistance
	if mods["Krastorio2"] then
		add_resistance("kr-railgun-turret", "ammo-turret", r)
		add_resistance("kr-rocket-turret", "ammo-turret", r)
	end
	]]

	if mods["Soviet-Tesla-Coils"] then
		add_resistance("soviet-tesla-coil", "electric-turret", r)
	end

	if mods["vtk-cannon-turret"] then
		add_resistance("vtk-cannon-turret", "ammo-turret", r)
		add_resistance("vtk-cannon-turret-heavy", "ammo-turret", r)
	end
end

-- assimilate science-tweaker
for _, lab in pairs(data.raw.lab) do
	lab.researching_speed = (lab.researching_speed or 1) * settings.startup["issy-speed-lab"].value
end

local items = { ["tiberium-science"] = true, ["research-data"] = true } -- Factorio-Tiberium and Atomic_Overhaul
for _, item in pairs(data.raw.tool) do
	if item.subgroup == "science-pack" then
		items[item.name] = true
	end
end
for _, recipe in pairs(data.raw.recipe) do
	if (recipe.results ~= nil and #recipe.results == 1 and
			(items[recipe.results[1][1]] or items[recipe.results[1].name])) or
			items[recipe.result]
	then
		log("modifying " .. recipe.name)
		recipe.energy_required = recipe.energy_required * settings.startup["issy-speed-science"].value
	end
end

-- match limitations of thorium modules and productivity modules
if mods["Atomic_Overhaul"] and settings.startup["issy-thorium-module"].value then
	data.raw.module["thorium-module-1"].limitation = data.raw.module["productivity-module"].limitation
	data.raw.module["thorium-module-2"].limitation = data.raw.module["productivity-module-2"].limitation
	data.raw.module["thorium-module-3"].limitation = data.raw.module["productivity-module-3"].limitation
end

-- reorder shortcuts
for name in string.gmatch(settings.startup["issy-reorder-shortcuts"].value, "([^%s]+)") do
	if data.raw.shortcut[name] ~= nil then
		data.raw.shortcut[name].order = "z"
	end
end

-- fix tank inventory size
if mods["Krastorio2"] and settings.startup["issy-shrink-inventories"].value then
	data.raw.car["tank"].inventory_size = 10
end

-- apply picker's bot scale to remnants/dying-particle
if mods["PickerTweaks"] then
	local scale = settings.startup['picker-adjustable-bot-scale'].value
	if scale ~= 1 then
		for _, type in pairs({ "construction-robot", "logistic-robot" }) do
			for _, entity in pairs(data.raw[type]) do
				if entity.dying_explosion ~= nil then
					local animations = data.raw.explosion[entity.dying_explosion].animations
					for i = 1, #animations do
						animations[i].scale = (animations[i].scale or 1) * scale
						local hr = animations[i].hr_version
						if hr ~= nil then hr.scale = (hr.scale or 1) * scale end
					end
					local corpse = data.raw[mods["robot_attrition"] and "simple-entity" or "corpse"][entity.name .. "-remnants"]
					if corpse ~= nil then
						local animations = corpse.animations or corpse.animation
						for i = 1, #animations do
							animations[i].scale = (animations[i].scale or 1) * scale
							local hr = animations[i].hr_version
							if hr ~= nil then hr.scale = (hr.scale or 1) * scale end
						end
					end
					local dying = data.raw["optimized-particle"][entity.name .. "-dying-particle"]
					if dying ~= nil then
						for _, animations in pairs({ dying.pictures, dying.shadows }) do
							for i = 1, #animations do
								animations[i].scale = (animations[i].scale or 1) * scale
								local hr = animations[i].hr_version
								if hr ~= nil then hr.scale = (hr.scale or 1) * scale end
							end
						end
					end
				end
			end
		end
	end
end

-- expensive landfill
if settings.startup["issy-expensive-landfill"].value then
	if mods["space-exploration"] then
		data.raw.recipe["landfill"].energy_required = 20
		mod_ingredient_count("landfill", "stone", 400)
		data.raw.recipe["landfill-sand"].normal.energy_required = 20
		data.raw.recipe["landfill-sand"].expensive.energy_required = 20
		mod_ingredient_count("landfill-sand", "sand", 1600)
		data.raw.recipe["landfill-se-scrap"].normal.energy_required = 20
		data.raw.recipe["landfill-se-scrap"].expensive.energy_required = 20
		mod_ingredient_count("landfill-se-scrap", "se-scrap", 800)
		data.raw.recipe["landfill-iron-ore"].normal.energy_required = 20
		data.raw.recipe["landfill-iron-ore"].expensive.energy_required = 20
		mod_ingredient_count("landfill-iron-ore", "iron-ore", 400)
		data.raw.recipe["landfill-copper-ore"].normal.energy_required = 20
		data.raw.recipe["landfill-copper-ore"].expensive.energy_required = 20
		mod_ingredient_count("landfill-copper-ore", "copper-ore", 400)
		data.raw.recipe["landfill-se-iridium-ore"].normal.energy_required = 20
		data.raw.recipe["landfill-se-iridium-ore"].expensive.energy_required = 20
		mod_ingredient_count("landfill-se-iridium-ore", "se-iridium-ore", 200)
		data.raw.recipe["landfill-se-holmium-ore"].normal.energy_required = 20
		data.raw.recipe["landfill-se-holmium-ore"].expensive.energy_required = 20
		mod_ingredient_count("landfill-se-holmium-ore", "se-holmium-ore", 400)
		data.raw.recipe["landfill-se-beryllium-ore"].normal.energy_required = 20
		data.raw.recipe["landfill-se-beryllium-ore"].expensive.energy_required = 20
		mod_ingredient_count("landfill-se-beryllium-ore", "se-beryllium-ore", 400)
		if mods["bzaluminum"] then
			data.raw.recipe["landfill-aluminum-ore"].energy_required = 20
			mod_ingredient_count("landfill-aluminum-ore", "aluminum-ore", 400)
		end
		if mods["bzcarbon"] then
			data.raw.recipe["landfill-flake-graphite"].energy_required = 20
			mod_ingredient_count("landfill-flake-graphite", "flake-graphite", 400)
		end
		if mods["bzlead"] then
			data.raw.recipe["landfill-lead-ore"].energy_required = 20
			mod_ingredient_count("landfill-lead-ore", "lead-ore", 400)
		end
		if mods["bztin"] then
			data.raw.recipe["landfill-tin-ore"].energy_required = 20
			mod_ingredient_count("landfill-tin-ore", "tin-ore", 400)
		end
		if mods["bztitanium"] then
			data.raw.recipe["landfill-titanium-ore"].energy_required = 20
			mod_ingredient_count("landfill-titanium-ore", "titanium-ore", 400)
		end
		if mods["bztungsten"] then
			data.raw.recipe["landfill-tungsten-ore"].energy_required = 20
			mod_ingredient_count("landfill-tungsten-ore", "tungsten-ore", 400)
		end
		if mods["bzzirconium"] then
			data.raw.recipe["landfill-zircon"].energy_required = 20
			mod_ingredient_count("landfill-zircon", "zircon", 400)
		end
	end
end

if not mods["cheaper-logistics"] then
	-- reduce underground belt cost
	local count = get_ingredient_count("underground-belt", "transport-belt")
	for _, name in pairs({ "fast", "express" }) do
		mod_ingredient_count(name .. "-underground-belt", name .. "-transport-belt", count)
	end
	if mods["Krastorio2"] then
		for _, name in pairs({ "kr-advanced", "kr-superior" }) do
			mod_ingredient_count(name .. "-underground-belt", name .. "-transport-belt", count)
		end
	end
end

-- reduce gun equipment range
if mods["GunEquipment"] then
	for _, eq in pairs(data.raw["active-defense-equipment"]) do
		if string.match(eq.name, "^personal%-turret%-") then
			eq.attack_parameters.range = 14
		end
	end
end
