local fastforward
fastforward = {
	enabled = settings.global["issy-fastforward"].value,
	speed = settings.global["issy-fastforward-speed"].value,
	disable = function(player)
		if game.speed == 1 then return end
		game.speed = 1
		if player then
			game.print({ "issy.fastforward-disabled", player.name })
		else
			game.print({ "issy.fastforward-disabled-global" })
		end
	end,
	enable = function(player)
		if fastforward.enabled then
			game.speed = fastforward.speed
			if player then
				game.print({ "issy.fastforward-enabled", player.name })
			else
				game.print({ "issy.fastforward-changed" })
			end
		else
			if player then player.print({ "issy.fastforward-disabled-global" }) end
		end
	end,
	toggle = function(player)
		if game.speed ~= 1 then
			fastforward.disable(player)
		else
			fastforward.enable(player)
		end
	end
}
script.on_event("issy-fastforward", function(event)
	fastforward.toggle(game.get_player(event.player_index))
end)

script.on_event(defines.events.on_runtime_mod_setting_changed, function(event)
	if event.setting == "issy-fastforward" then
		fastforward.enabled = settings.global["issy-fastforward"].value
		if not fastforward.enabled then fastforward.disable() end
	elseif event.setting == "issy-fastforward-speed" then
		fastforward.speed = settings.global["issy-fastforward-speed"].value
		if game.speed ~= 1 then fastforward.enable() end
	end
end)

script.on_event("issy-minimap", function(event)
	game.get_player(event.player_index).game_view_settings.show_minimap =
			not game.get_player(event.player_index).game_view_settings.show_minimap
end)

script.on_event("issy-quickbar", function(event)
	game.get_player(event.player_index).game_view_settings.show_controller_gui =
			not game.get_player(event.player_index).game_view_settings.show_controller_gui
end)

function on_init()
	on_load()
end

function on_load()
	commands.add_command("fix-tech", { "issy.fix-tech-description" }, function(c)
		for name, force in pairs(game.forces) do
			if not (name == "enemy" or name == "neutral") then
				force.reset_technology_effects()
			end
		end
	end)

	commands.add_command("toggle-queue", { "issy.toggle-queue-description" }, function(c)
		local player = game.get_player(c.player_index)
		local new_state = not player.force.research_queue_enabled
		player.force.research_queue_enabled = new_state
		if player.force.research_queue_enabled == new_state then
			player.force.print({ "issy.toggle-queue-" .. (new_state and "enabled" or "disabled"), player.name })
		else
			player.print({ "issy.toggle-queue-failed" })
		end
	end)
end

script.on_init(on_init)
script.on_load(on_load)

local function clear_tiles(surface, list)
	local tiles = {
		"stone-path", "concrete", "hazard-concrete-left", "hazard-concrete-right",
		"refined-concrete", "refined-hazard-concrete-left", "refined-hazard-concrete-right",
		"transport-drone-road", "transport-drone-road-better", "transport-drone-proxy-tile",
		"kr-black-reinforced-plate", "kr-white-reinforced-plate", "stone-covering", "carbon-covering",
		"early-logistics-provider", "early-logistics-storage", "early-logistics-vehicle",
		"tarmac", "rough-stone-path", "omnite-brick"
	}
	for tile, _ in pairs(game.tile_prototypes) do
		if string.sub(tile, 1, 5) == "Arci-" then table.insert(tiles, tile) end
	end
	for i = 1, #tiles do tiles[tiles[i]] = true end
	local entities = {}
	for name, entity in pairs(game.entity_prototypes) do
		if entity.type == "tree" or entity.count_as_rock_for_filtered_deconstruction then
			table.insert(entities, name)
			if entity.corpses ~= nil then
				for name, _ in pairs(entity.corpses) do table.insert(entities, name) end
			end
		end
	end
	if settings.startup["issy-tile-cliffs"].value then
		for name, entity in pairs(game.get_filtered_entity_prototypes { { filter = "type", type = "cliff" } }) do
			table.insert(entities, name)
		end
	end

	clear_tiles = function(surface, list)
		for pos, tile in pairs(list) do
			if tiles[tile.name] ~= nil then
				for _, entity in pairs(surface.find_entities_filtered {
					area = {
						{ pos.x - 0.5, pos.y - 0.5 },
						{ pos.x + 2,   pos.y + 2   }
					},
					name = entities
				}) do
					for name, force in pairs(game.forces) do
						if not (name == "enemy" or name == "neutral" or name == "AbandonedRuins:enemy") then
							entity.order_deconstruction(force)
						end
					end
				end
			end
		end
	end
	return clear_tiles(surface, list)
end

script.on_event({
	defines.events.on_player_built_tile,
	defines.events.on_robot_built_tile,
	defines.events.script_raised_set_tiles
}, function(event)
	local surface = game.get_surface(event.surface_index)
	local tiles = {}
	for i = 1, #event.tiles do
		tiles[event.tiles[i].position] = surface.get_tile(event.tiles[i].position)
	end

	clear_tiles(surface, tiles)
end)
