-- match boiler energy consumption
if mods["Krastorio2"] then
	local energy = data.raw.boiler["boiler"].energy_consumption

	if settings.startup["issy-electric-boiler"].value then
		data.raw.boiler["electric-boiler"].energy_consumption = energy
	end

	if mods["bzgas"] then
		data.raw.boiler["gas-boiler"].energy_consumption = energy
	end
end

-- increase rocket launcher range
data.raw.gun["rocket-launcher"].attack_parameters.range = 60
data.raw.gun["spidertron-rocket-launcher-1"].attack_parameters.range = 60
data.raw.gun["spidertron-rocket-launcher-2"].attack_parameters.range = 60
data.raw.gun["spidertron-rocket-launcher-3"].attack_parameters.range = 60
data.raw.gun["spidertron-rocket-launcher-4"].attack_parameters.range = 60

-- increase spidertron's grid size
data.raw["equipment-grid"]["spidertron-equipment-grid"].height = 13

if mods["Krastorio2"] then
	data.raw["equipment-grid"]["kr-spidertron-equipment-grid"].height = 13
end

-- earlier transport drones with bzaluminum/bzgas
if mods["Transport_Drones"] then
	if mods["bzaluminum"] then
		mod_ingredient("transport-drone", "aluminum-6061", "aluminum-plate", 5)
	end
	local prereqs = data.raw.technology["transport-system"].prerequisites
	for i = #prereqs, 1, -1 do
		if prereqs[i] == "oil-processing" or prereqs[i] == "basic-alloys" then
			table.remove(prereqs, i)
		end
	end
end

-- reduce inventory sizes
if settings.startup["issy-shrink-inventories"].value then
	if mods["ammo-loader"] or mods["ammo-loader-Revise"] then
		data.raw.container["ammo-loader-chest"].inventory_size = 16
		for _, i in pairs({ "storage", "requester", "passive-provider" }) do
			data.raw["logistic-container"]["ammo-loader-chest-" .. i].inventory_size = 16
		end
	end

	if mods["warptorio2_expansion"] then
		data.raw["linked-container"]["wpe_linked_chest"].inventory_size = 16
	end
end

-- fix Personal-Roboport-Mk3's categories with K2
if mods["Personal-Roboport-Mk3"] and mods["Krastorio2"] then
	for i = 3, 6 do
		data.raw["roboport-equipment"]["personal-roboport-mk" .. i .. "-equipment"].categories = {
			"robot-interaction-equipment" }
	end
end

-- add missing effects to tiberium rockets
if mods["Factorio-Tiberium"] then
	local effects = data.raw.projectile["tiberium-rocket"].action.action_delivery.target_effects
	effects[4].entity_name = "medium-scorchmark-tintable"
	effects[5].action.radius = 8
	table.insert(effects, 4, {
		type = "show-explosion-on-chart",
		scale = 0.5
	})
	table.insert(effects, 4, {
		type = "destroy-decoratives",
		from_render_layer = "decorative",
		to_render_layer = "object",
		include_soft_decoratives = true, -- soft decoratives are decoratives with grows_through_rail_path = true
		include_decals = false,
		invoke_decorative_trigger = true,
		decoratives_with_trigger_only = false, -- if true, destroys only decoratives that have trigger_effect set
		radius = 3.5                         -- large radius for demostrative purposes
	})
	table.insert(effects, 4, {
		type = "invoke-tile-trigger",
		repeat_count = 1
	})
end
