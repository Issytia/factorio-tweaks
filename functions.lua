function add_resistances(e, t, rs)
	local resistances = data.raw[t][e].resistances
	if resistances == nil then
		data.raw[t][e].resistances = rs
		return
	end

	for _, r in pairs(rs) do
		local found = false

		for _, s in pairs(resistances) do
			if r.type == s.type then
				found = true
				if r.decrease ~= nil then
					if s.decrease ~= nil then
						s.decrease = s.decrease + r.decrease
					else
						s.decrease = r.decrease
					end
				end
				if r.percent ~= nil then
					if s.percent ~= nil then
						s.percent = s.percent * (1 + (r.percent / 100))
					else
						s.percent = r.percent
					end
				end
				break
			end
		end

		if not found then table.insert(resistances, r) end
	end
end

function add_resistance(e, t, r)
	return add_resistances(e, t, { r })
end

function add_productivity(r)
	if type(r) ~= "table" then r = { r } end
	for _, recipe in pairs(r) do
		for _, module in pairs(data.raw.module) do
			if module.effect then
				for name, effect in pairs(module.effect) do
					if name == "productivity" and effect.bonus > 0 and module.limitation and #module.limitation > 0 then
						table.insert(module.limitation, recipe)
					end
				end
			end
		end
	end
end

function get_ingredient_count(recipe, item)
	recipe = data.raw.recipe[recipe]
	local ingr
	if recipe.normal ~= nil or recipe.expensive ~= nil then
		if recipe.normal then
			ingr = recipe.normal.ingredients
		else
			ingr = recipe.expensive.ingredients
		end
	else
		ingr = recipe.ingredients
	end
	local count = 0
	for i = 1, #ingr do
		if ingr[i].name ~= nil then
			if ingr[i].name == item then
				count = count + ingr[i].amount
			end
		else
			if ingr[i][1] == item then
				count = count + ingr[i][2]
			end
		end
	end
	return count
end

function mod_ingredient_count(recipe, item, count)
	return mod_ingredient(recipe, item, item, count)
end

function mod_ingredient_counts(recipe, counts)
	for _, a in pairs(counts) do
		mod_ingredient_count(recipe, a[1], a[2])
	end
end

function mod_ingredient_count_tech(tech, item, count)
	return mod_ingredient(tech, item, item, count, true)
end

function mod_ingredient_tech(tech, from, to, count)
	return mod_ingredient(tech, from, to, count, true)
end

function mod_ingredient(name, from, to, count, tech)
	local recipe, ingr, f
	if tech then
		recipe = data.raw.technology[name]
	else
		recipe = data.raw.recipe[name]
	end
	if recipe.normal ~= nil or recipe.expensive ~= nil then
		if recipe.normal then
			ingr = recipe.normal.ingredients or recipe.normal.unit.ingredients
		else
			ingr = recipe.expensive.ingredients or recipe.expensive.unit.ingredients
		end
	else
		ingr = recipe.ingredients or recipe.unit.ingredients
	end
	for i = #ingr, 1, -1 do
		if ingr[i].name ~= nil then
			if ingr[i].name == from then
				ingr[i].name = to
				f = true
				if count and count ~= 0 then
					ingr[i].amount = count
				elseif from == to then
					table.remove(ingr, i)
				end
			end
		else
			if ingr[i][1] == from then
				ingr[i][1] = to
				f = true
				if count and count ~= 0 then
					ingr[i][2] = count
				elseif from == to then
					table.remove(ingr, i)
				end
			end
		end
	end
	if not f then log("mod_ingredient: " .. name .. " doesn't have " .. from) end
end

function add_prereq(tech, prereq)
	local p = data.raw.technology[tech].prerequisites
	for i = 1, #p do
		if p[i] == prereq then
			log("add_prereq: " .. tech .. " already has " .. prereq)
			return
		end
	end
	table.insert(p, prereq)
end

function remove_prereq(tech, prereq)
	local p, f = data.raw.technology[tech].prerequisites, false
	for i = #p, 1, -1 do
		if p[i] == prereq then
			table.remove(p, i)
			f = true
		end
	end
	if not f then log("remove_prereq: " .. tech .. " doesn't have " .. prereq) end
end
