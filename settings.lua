data:extend({
	{
		name = "issy-cheap-wires",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-cheaper-logistics",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-coal-liquefaction",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-electric-boiler",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-expensive-landfill",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-explosives-stack",
		type = "int-setting",
		default_value = 100,
		minimum_value = 0,
		setting_type = "startup",
		order = "tile-explosives"
	},
	{
		name = "issy-fastforward",
		type = "bool-setting",
		default_value = true,
		setting_type = "runtime-global"
	},
	{
		name = "issy-fastforward-speed",
		type = "double-setting",
		default_value = 5.0,
		minimum_value = 0.01,
		setting_type = "runtime-global"
	},
	{
		name = "issy-hazard-concrete",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-no-research-speed",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup",
	},
	{
		name = "issy-power-pole",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-reorder-shortcuts",
		type = "string-setting",
		default_value = [[
			UB_admin_area_tool_shortcut kr-creep-collector kr-jackhammer ltnm-toggle-gui
			milestones_toggle_gui picker-auto-circuit picker-belt-highlighter pv-toggle-hover qis-search
			rampant-evolution--info rb-search rcalc-get-selection-tool rcalc-inserter-selector informatron
			spidertron-enhancements-recall-shortcut toggle-alt-mode
			flashlight-toggle toggle-personal-logistic-requests big-zoom minimap helmod-shortcut
			yarm-selector aa-select-zone undo copy cut paste give-blueprint give-deconstruction-planner
			give-blueprint-book picker-bp-prioritizer-shortcut give-upgrade-planner shield-generator-switch
			picker-bp-updater picker-wire-cutter picker-pipe-clamper toggle-night-vision-equipment
			picker-camera artillery-jammer-tool spidertron-logistics spidertron-logistic-requests
			train-mode-toggle unit-remote-control path-remote-control shuttle-train-shortcut
			import-string bpsb-toggle-gui-sb-toggle-shortcut bpsb-sbr-aluminum-ore bpsb-sbr-crude-oil
			bpsb-sbr-deep_oil bpsb-sbr-gas bpsb-sbr-graphite bpsb-sbr-imersite bpsb-sbr-lead-ore
			bpsb-sbr-mineral-water bpsb-sbr-rare-metals bpsb-sbr-salt bpsb-sbr-tin-ore bpsb-sbr-titanium-ore
			bpsb-sbr-tungsten-ore bpsb-sbr-zircon bpsb-sbr-coal bpsb-sbr-copper-ore bpsb-sbr-iron-ore
			bpsb-sbr-stone bpsb-sbr-uranium-ore
		]],
		setting_type = "startup"
	},
	{
		name = "issy-shrink-inventories",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-small-nightvision",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	},
	{
		name = "issy-speed-lab",
		type = "double-setting",
		default_value = 1,
		setting_type = "startup"
	},
	{
		name = "issy-speed-science",
		type = "double-setting",
		default_value = 1,
		setting_type = "startup"
	},
	{
		name = "issy-tile-brick",
		type = "bool-setting",
		default_value = false,
		setting_type = "startup",
		order = "tile-stack-b"
	},
	{
		name = "issy-tile-landfill",
		type = "bool-setting",
		default_value = false,
		setting_type = "startup",
		order = "tile-stack-c"
	},
	{
		name = "issy-tile-stack",
		type = "int-setting",
		default_value = 1000,
		minimum_value = 0,
		setting_type = "startup",
		order = "tile-stack-a"
	},
	{
		name = "issy-tile-clear",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup",
		order = "tile-clear"
	},
	{
		name = "issy-tile-cliffs",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup",
		order = "tile-clear-cliffs"
	},
	{
		name = "issy-turret-fire-resistance",
		type = "bool-setting",
		default_value = true,
		setting_type = "startup"
	}
})

if mods["Atomic_Overhaul"] then
	data:extend({
		{
			name = "issy-thorium-module",
			type = "bool-setting",
			default_value = true,
			setting_type = "startup",
			order = "z"
		}
	})
end
