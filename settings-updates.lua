local function default(t, s, v)
	data.raw[t .. "-setting"][s].default_value = v
end
local function hide(t, s, v)
	data.raw[t .. "-setting"][s].hidden = true
	if t == "bool" then
		data.raw[t .. "-setting"][s].forced_value = v or data.raw[t .. "-setting"][s].default_value
	else
		data.raw[t .. "-setting"][s].allowed_values = { v or data.raw[t .. "-setting"][s].default_value }
	end
end

if mods["aai-industry"] then
	default("bool", "tint-ghosts", false)
end

if mods["ArmouredBiters"] and not mods["Rampant"] then
	default("bool", "ab-enable-nest", true)
	default("bool", "ab-enable-moisture-check", true)
end

if mods["artefact_patched"] then
	default("double", "artefact_patched-uranium-ore-fuel", 20)
end

if mods["Automatic_Train_Painter"] then
	default("bool", "loc-eqpm-grid", true)
	default("int", "loc-eqpm-grid-h", 6)
	default("bool", "atp-ltn-paint", true)
	default("bool", "paint-cargo-wagon", true)
	default("bool", "paint-fluid-wagon", true)
end

if mods["bzaluminum"] then
	default("bool", "bzaluminum-byproduct", true)
	default("bool", "bzaluminum-starting-items", true)
end
if mods["bzcarbon"] then
	default("string", "bzcarbon-enable-carbon-black", "yes")
	default("string", "bzcarbon-enable-fullerenes", "yes")
	default("string", "bzcarbon-enable-rough-diamond", "yes")
	default("string", "bzcarbon-enable-flake-graphite", "yes")
	default("string", "bzcarbon-reuse", "yes")
	if mods["bzchlorine"] then
		default("string", "bzcarbon-enable-carbon-fiber", "yes")
	end
end
if mods["bzchlorine"] then
	default("bool", "bzchlorine-salt", true)
	default("string", "bzchlorine-early-salt", "mining")
	default("string", "bzchlorine-more-intermediates", "yes")
end
if mods["bzfoundry"] then
	default("string", "bzfoundry-hydrocarbon", "coke")
	default("bool", "bzfoundry-smelt", false)
	default("string", "bzfoundry-plates", "yes")
	if not mods["Krastorio2"] then
		default("bool", "bzfoundry-woodcoke", true)
	end
end
if mods["bzgas"] then
	default("string", "bzgas-more-intermediates", "phenol")
	default("bool", "bzgas-force-spawn", true)
	if not mods["Krastorio2"] then
		default("bool", "bzgas-finite", mods["InfiniteResourcesNormalYieldDepletion"] ~= nil)
	end
end
if mods["bzgold"] then
	default("bool", "bzgold-byproduct", true)
	default("bool", "bzgold-palladium", true)
	default("bool", "bzgold-platinum", true)
	default("bool", "bzgold-silver", true)
	default("bool", "bzgold-catalysis", true)
	default("bool", "bzgold-alchemy", true)
end
if mods["bzlead"] then
	default("bool", "bzlead-byproduct", true)
	default("string", "bzlead-more-ammo", "yes")
	default("string", "bzlead-more-entities", (not mods["IndustrialRevolution"] and not mods["IndustrialRevolution3"]) and "yes" or "no")
end
if mods["bzsettings"] then
	default("bool", "bz-all-intermediates", true)
	hide("bool", "bz-tabula-rasa", false)
end
if mods["bzsilicon"] then
	default("string", "bzsilicon-more-intermediates", "more")
end
if mods["bztin"] then
	default("string", "bztin-more-intermediates", "bronze")
end
if mods["bztungsten"] then
	default("string", "bztungsten-more-intermediates", "cuw")
	default("string", "bztungsten-avoid-military", "yes")
	if not mods["Krastorio2"] then
		default("bool", "bztungsten-starting-patch", false)
	end
	if mods["space-exploration"] then
		if data.raw["bool-setting"]["bztungsten-se-sintering"] ~= nil then
			default("bool", "bztungsten-se-sintering", true)
		end
	end
end
if mods["bzzirconium"] then
	default("bool", "bzzirconium-byproduct", true)
	default("string", "bzzirconium-enable-intermediates", "lds")
	default("bool", "bzzirconium-early", true)
	default("bool", "bzzirconium-ammo", true)
end

if mods["cargo-ships"] then
	default("string", "oil_richness", "good")
end

if mods["Clockwork"] then
	default("bool", "Clockwork-flares-simple", true)
	default("int", "Clockwork-darknight-percent", 75)
	default("bool", "Clockwork-multisurface", true)
end

if mods["Cold_biters"] then
	default("bool", "cb-enable-cold-warfare", false)
	default("double", "cb-DamageScaler", 0.7)
	default("double", "cb-HealthScaler", 0.8)
end

if mods["combat-mechanics-overhaul"] then
	default("bool", "pools-affect-structures", true)
end

if mods["combinator-toggle"] then
	default("bool", "combinator-toggle-power-switch-setting", true)
end

if mods["Concretexture"] then
	default("bool", "stone-path-concrete", true)
	default("string", "concrete-texture", "patches")
	default("string", "refined-texture", "smooth")
end

if mods["ConfigLampTimes"] then
	default("double", "config-lamp-times-brightness-start-1", 0.85)
	default("double", "config-lamp-times-brightness-end-1", 0.75)
end

if mods["CreepCleaner"] then
	default("bool", "cc-krastorio2-creep", true)
end

if mods["DeadlockBlackRubberBelts"] then
	default("bool", "ir2rb-arrows", true)
	default("bool", "ir2rb-arrows-doubled", true)
	default("bool", "ir2rb-rails", true)
end

if mods["dim_lamps"] then
	default("string", "brightness_multiplier", 0.75)
end

if mods["discovery_tree"] then
	default("string", "discovery-tree-mode", "all")
end

if mods["Explosive_biters"] then
	default("double", "eb-DamageScaler", 0.7)
	default("double", "eb-HealthScaler", 0.8)
end

if mods["Factorio-Tiberium"] then
	default("int", "tiberium-blue-saturation-point", 25)
	default("bool", "tiberium-byproduct-1", true)
	default("bool", "tiberium-byproduct-2", true)
	default("bool", "tiberium-byproduct-direct", true)
end

if mods["GCKI"] then
	default("bool", "GCKI-flying_text", true)
	default("bool", "GCKI-guiclaim_locked_vehicle", true)
	default("bool", "GCKI-autoclaim_locked_vehicle", true)
	default("string", "GCKI-select_orientation_from", "player")
	default("bool", "GCKI-long_reach", true)
end

if mods["Gun_Turret_Alerts"] then
	default("bool", "gun-turret-alerts-car-enabled", false)
	default("int", "gun-turret-alerts-threshold", 5)
end

if mods["Hovercrafts"] then
	default("bool", "hovercraft-grid", true)
	default("string", "grid-hcraft", "4x4")
	default("string", "grid-mcraft", "6x6")
end

if mods["IndustrialRevolution3"] then
	default("string", "ir-laser-colour", "magenta")
	default("string", "ir-ghost-tint", "magenta")
	default("string", "ir-transfer-plate-style", "2")
	default("string", "ir-stone-wall-style", "on")
end
if mods["IndustrialRevolution3LoadersStacking"] then
	default("string", "ir3ls-batch-mode", "batch")
end

if mods["InfiniteResourcesNormalYieldDepletion"] then
	default("int", "InfiniteResourcesNormalYieldDepletion_minimum", 10)
	default("int", "InfiniteResourcesNormalYieldDepletion_normal", 200)
	default("string", "InfiniteResourcesNormalYieldDepletion_exclusions", "steam-fissure,dirty-steam-fissure,natural-gas-fissure,fossil-gas-fissure,sulphur-gas-fissure")
end

if mods["informatron"] then
	default("bool", "informatron-show-overhead-button", false)
	default("bool", "informatron-show-at-start", false)
end

if mods["InserterFuelLeech"] then
	default("int", "inserter-fuel-leech-min-fuel-items", 3)
	default("bool", "inserter-fuel-leech-self-refuel-cheat-enabled", true)
end

if mods["Krastorio2"] then
	default("string", "kr-stack-size", "No changes")
	default("bool", "kr-loaders", false)
	default("bool", "kr-containers", false)
	default("bool", "kr-electric-poles-changes", false)
	default("bool", "kr-pipes-and-belts-changes", false)
	default("bool", "kr-more-realistic-weapon-auto-aim", true)
	hide("string", "kr-shelter-tint")
end

if mods["Kux-OrbitalIonCannon"] then
	default("string", "ion-cannon-voice-style", "TiberianSunCABAL")
end

if mods["laser_fix"] then
	default("double", "laserfix-r", 0.929)
	default("double", "laserfix-g", 0.386)
	default("double", "laserfix-b", 0.514)
	default("double", "laserfix-glow-r", 0.4645)
	default("double", "laserfix-glow-g", 0.193)
	default("double", "laserfix-glow-b", 0.257)
end

if mods["LogisticTrainNetwork"] then
	default("string", "ltn-interface-console-level", "1")
	default("int", "ltn-dispatcher-requester-threshold", 100000000)
	default("int", "ltn-dispatcher-provider-threshold", 100000000)
	default("int", "ltn-dispatcher-depot-inactivity(s)", 1)
	default("int", "ltn-dispatcher-stop-timeout(s)", 0)
	default("int", "ltn-dispatcher-delivery-timeout(s)", 3600)
	default("double", "ltn-depot-fluid-cleaning", 1)
end
if mods["LTN_Combinator_Modernized"] then
	default("string", "ltnc-disable-built-combinators", "off")
end

if mods["Milestones"] then
	default("bool", "milestones_compact_list", true)
end

if mods["miner-placement-aid"] then
	default("bool", "placement-aid-draw-on-top-of-player", true)
end

if mods["Nearby-Ammo-Count"] then
	default("string", "nearby-ammo-display-mode", "shot")
end

if mods["Nuclear Fuel"] then
	default("string", "nuclear-fuel-cycle-type", "alternative")
	default("bool", "nuclear-fuel-geiger-tick", true)
end

if mods["PickerBeltTools"] then
	default("bool", "picker-tool-wire-cutter", false)
	default("bool", "picker-tool-rewire", false)
end
if mods["PickerBlueprinter"] then
	default("bool", "picker-simple-blueprint", false)
	default("bool", "picker-tool-bp-updater", false)
	default("bool", "picker-bp-prioritizer", false)
	default("bool", "picker-bp-snap", false)
end
if mods["PickerExtended"] or mods["PickerExtended_SE"] then
	default("bool", "picker-revive-selected-ghosts-entity", false)
	default("bool", "picker-revive-selected-ghosts-tile", false)
	default("bool", "picker-allow-multiple-craft", true)
	default("bool", "picker-search-light", true)
	default("bool", "picker-alt-mode-default", true)
	default("string", "picker-chat-color", "pink")
	default("string", "picker-character-color", "pink")
	default("bool", "picker-tool-tape-measure", false)
	default("bool", "picker-tool-camera", false)
end
if mods["PickerInventoryTools"] or mods["PickerInventoryTools_SE"] then
	default("bool", "picker-auto-sort-inventory", false)
	default("bool", "picker-item-zapper-all", true)
	default("bool", "picker-copy-between-surfaces", true)
	default("bool", "picker-autodeconstruct", false)
	default("bool", "picker-autodeconstruct-target", false)
end
if mods["PickerTweaks"] then
	default("bool", "picker-noalt-logistic-robots", false)
	default("bool", "picker-noalt-construction-robots", false)
	default("bool", "picker-fireproof-construction-robots", true)
	default("bool", "picker-unminable-construction-robots", true)
	default("bool", "picker-unminable-logistic-robots", true)
	default("double", "picker-adjustable-bot-scale", 0.5)
	default("bool", "picker-smaller-tree-box", true)
	default("bool", "picker-squeak-through", true)
	default("double", "picker-reacher-drop-item-distance", 6)
	default("double", "picker-reacher-build-distance", 6)
	default("double", "picker-reacher-reach-distance", 6)
	default("bool", "picker-small-unplugged-icon", true)
	default("string", "picker-iondicators-line", "purple")
	default("string", "picker-iondicators-arrow", "purple")
	default("string", "picker-ghost-tint", "pink")
	default("bool", "picker-fireproof-rail-signals", true)
	default("bool", "picker-generic-vehicle-grids", false)
	default("bool", "picker-free-circuit-wires", true)
	hide("int", "picker-tile-stack", 0)
	default("bool", "picker-show-bots-on-map", true)
	default("bool", "picker-return-ingredients", true)
end
if mods["PickerVehicles"] then
	default("bool", "picker-train-honk", false)
	default("double", "picker-temporary-stop-wait-time", 120)
	default("double", "picker-inactivity-wait-condition-default", 60)
	default("bool", "picker-naked-rails", false)
end

if mods["qol_research"] then
	default("bool", "qol-modpack-compatibility-enabled", false)

	local config = [[1,17,crafting-speed,250,automation-science-pack,logistic-science-pack,500,chemical-science-pack,750,production-science-pack,1000,space-science-pack,1000+500*2.5^L,inventory-size,utility-science-pack,mining-speed,1500,movement-speed,player-reach,5,1,5,2,25,0,0,2,30,1,1,3,2,25,1,1,4,5,30,2,1,3,1,4,2,25,1,1,6,7,45,3,1,3,1,4,1,6,2,25,1,1,8,9,45,4,1,3,1,4,1,6,1,8,0,25,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,12,5,3,10,0,0,2,30,1,1,3,3,10,1,1,4,5,30,2,1,3,1,4,3,20,1,1,6,7,45,3,1,3,1,4,1,6,3,20,1,1,13,9,45,4,1,3,1,4,1,6,1,13,0,20,1,1,10,11,60,5,1,3,1,4,1,6,1,13,1,10,14,5,1,20,0,0,2,30,1,1,3,1,20,1,1,4,5,30,2,1,3,1,4,1,20,1,1,6,7,45,3,1,3,1,4,1,6,1,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,20,1,1,10,15,60,5,1,3,1,4,1,6,1,8,1,10,16,5,1,10,0,0,2,30,1,1,3,1,10,1,1,4,5,30,2,1,3,1,4,1,10,1,1,6,7,45,3,1,3,1,4,1,6,1,10,1,1,13,9,45,4,1,3,1,4,1,6,1,13,1,10,1,1,10,15,60,5,1,3,1,4,1,6,1,13,1,10,17,5,2,3,0,0,2,30,1,1,3,2,3,1,1,4,5,30,2,1,3,1,4,2,3,1,1,6,7,45,3,1,3,1,4,1,6,2,3,1,1,8,9,45,4,1,3,1,4,1,6,1,8,12,3,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10]]
	if mods["space-exploration"] then
		config = [[1,29,crafting-speed,250,automation-science-pack,logistic-science-pack,500,chemical-science-pack,750,space-science-pack,1000,se-biological-science-pack-1,10+90*(L-1),se-space-catalogue-biological-2,100+20*2.5^L,se-biological-science-pack-2,se-space-catalogue-biological-3,250+50*2.5^L,se-biological-science-pack-3,se-space-catalogue-biological-4,500+100*2.5^L,se-biological-science-pack-4,se-deep-space-science-pack-1,1000+500*2.5^L,inventory-size,mining-speed,10,100,1500,movement-speed,player-reach,5,1,9,2,25,0,0,2,30,1,1,3,2,25,1,1,4,5,30,2,1,3,1,4,2,25,1,1,6,7,45,3,1,3,1,4,1,6,2,25,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,25,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,2,25,1,1,12,13,60,5,1,3,1,4,1,6,1,8,1,14,2,25,1,1,15,16,60,5,1,3,1,4,1,6,1,8,1,17,2,25,1,1,18,19,60,5,1,3,1,4,1,6,1,8,1,20,0,25,1,1,21,22,60,6,1,3,1,4,1,6,1,8,1,20,1,21,23,9,2,10,0,0,2,30,1,1,3,2,10,1,1,4,5,30,2,1,3,1,4,2,20,1,1,6,7,45,3,1,3,1,4,1,6,2,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,20,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,2,20,1,1,12,13,60,5,1,3,1,4,1,6,1,8,1,14,2,20,1,1,15,16,60,5,1,3,1,4,1,6,1,8,1,17,2,20,1,1,18,19,60,5,1,3,1,4,1,6,1,8,1,20,0,20,1,1,21,22,60,6,1,3,1,4,1,6,1,8,1,20,1,21,24,9,1,20,0,0,2,30,1,1,3,1,20,1,1,4,5,30,2,1,3,1,4,1,20,1,1,6,7,45,3,1,3,1,4,1,6,1,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,20,1,1,10,25,60,5,1,3,1,4,1,6,1,8,1,10,1,10,1,1,12,26,60,5,1,3,1,4,1,6,1,8,1,14,1,10,1,1,15,2,60,5,1,3,1,4,1,6,1,8,1,17,1,15,1,1,18,7,60,5,1,3,1,4,1,6,1,8,1,20,1,15,1,1,21,27,60,6,1,3,1,4,1,6,1,8,1,20,1,21,28,9,1,10,0,0,2,30,1,1,3,1,10,1,1,4,5,30,2,1,3,1,4,1,10,1,1,6,7,45,3,1,3,1,4,1,6,1,10,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,10,1,1,10,25,60,5,1,3,1,4,1,6,1,8,1,10,1,10,1,1,12,26,60,5,1,3,1,4,1,6,1,8,1,14,1,10,1,1,15,2,60,5,1,3,1,4,1,6,1,8,1,17,1,15,1,1,18,7,60,5,1,3,1,4,1,6,1,8,1,20,1,15,1,1,21,27,60,6,1,3,1,4,1,6,1,8,1,20,1,21,29,9,2,3,0,0,2,30,1,1,3,2,3,1,1,4,5,30,2,1,3,1,4,2,3,1,1,6,7,45,3,1,3,1,4,1,6,2,3,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,3,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,2,3,1,1,12,13,60,5,1,3,1,4,1,6,1,8,1,14,2,3,1,1,15,16,60,5,1,3,1,4,1,6,1,8,1,17,2,3,1,1,18,19,60,5,1,3,1,4,1,6,1,8,1,20,2,3,1,1,21,22,60,6,1,3,1,4,1,6,1,8,1,20,1,21]]
	elseif mods["Krastorio2"] then
		config = [[1,23,crafting-speed,250,automation-science-pack,logistic-science-pack,500,chemical-science-pack,750,production-science-pack,1000,space-science-pack,1500,utility-science-pack,matter-tech-card,2000,advanced-tech-card,2000+500*2.5^L,singularity-tech-card,inventory-size,mining-speed,2500,movement-speed,player-reach,1500+500*2.5^L,5,1,7,2,25,0,0,2,30,1,1,3,2,25,1,1,4,5,30,2,1,3,1,4,2,25,1,1,6,7,45,3,1,3,1,4,1,6,2,25,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,25,1,1,10,11,60,4,1,12,1,8,1,10,1,13,2,25,1,1,10,14,60,5,1,12,1,8,1,10,1,13,1,15,0,25,1,1,10,16,60,6,1,12,1,8,1,10,1,13,1,15,1,17,18,7,2,10,0,0,2,30,1,1,3,2,10,1,1,4,5,30,2,1,3,1,4,2,20,1,1,6,7,45,3,1,3,1,4,1,6,2,20,1,1,12,9,45,4,1,3,1,4,1,6,1,12,2,20,1,1,10,11,60,4,1,12,1,8,1,10,1,13,2,20,1,1,10,14,60,5,1,12,1,8,1,10,1,13,1,15,0,20,1,1,10,16,60,6,1,12,1,8,1,10,1,13,1,15,1,17,19,7,1,20,0,0,2,30,1,1,3,1,20,1,1,4,5,30,2,1,3,1,4,1,20,1,1,6,7,45,3,1,3,1,4,1,6,1,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,20,1,1,10,11,60,4,1,12,1,8,1,10,1,13,1,25,1,1,10,14,60,5,1,12,1,8,1,10,1,13,1,15,1,25,1,1,10,20,60,6,1,12,1,8,1,10,1,13,1,15,1,17,21,7,1,10,0,0,2,30,1,1,3,1,10,1,1,4,5,30,2,1,3,1,4,1,10,1,1,6,7,45,3,1,3,1,4,1,6,1,10,1,1,12,9,45,4,1,3,1,4,1,6,1,12,1,10,1,1,10,11,60,4,1,12,1,8,1,10,1,13,1,10,1,1,10,14,60,5,1,12,1,8,1,10,1,13,1,15,1,15,1,1,10,20,60,6,1,12,1,8,1,10,1,13,1,15,1,17,22,7,2,3,0,0,2,30,1,1,3,2,3,1,1,4,5,30,2,1,3,1,4,2,3,1,1,6,7,45,3,1,3,1,4,1,6,2,3,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,3,1,1,10,11,60,4,1,12,1,8,1,10,1,13,4,3,1,1,10,23,60,5,1,12,1,8,1,10,1,13,1,15,6,3,3,1,10,16,60,6,1,12,1,8,1,10,1,13,1,15,1,17]]
	elseif mods["IndustrialRevolution3"] then
		config = [[1,18,crafting-speed,250,automation-science-pack,logistic-science-pack,500,chemical-science-pack,750,production-science-pack,1000,utility-science-pack,1500,space-science-pack,1500+500*2.5^L,inventory-size,mining-speed,2000,movement-speed,player-reach,5,1,6,2,25,0,0,2,30,1,1,3,2,25,1,1,4,5,30,2,1,3,1,4,2,25,1,1,6,7,45,3,1,3,1,4,1,6,2,25,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,25,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,0,25,1,1,12,13,60,6,1,3,1,4,1,6,1,8,1,10,1,12,14,6,3,10,0,0,2,30,1,1,3,3,10,1,1,4,5,30,2,1,3,1,4,3,20,1,1,6,7,45,3,1,3,1,4,1,6,3,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,3,20,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,0,20,1,1,12,13,60,6,1,3,1,4,1,6,1,8,1,10,1,12,15,6,1,20,0,0,2,30,1,1,3,1,20,1,1,4,5,30,2,1,3,1,4,1,20,1,1,6,7,45,3,1,3,1,4,1,6,1,20,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,20,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,1,20,1,1,12,16,60,6,1,3,1,4,1,6,1,8,1,10,1,12,17,6,1,10,0,0,2,30,1,1,3,1,10,1,1,4,5,30,2,1,3,1,4,1,10,1,1,6,7,45,3,1,3,1,4,1,6,1,10,1,1,8,9,45,4,1,3,1,4,1,6,1,8,1,15,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,1,20,1,1,12,16,60,6,1,3,1,4,1,6,1,8,1,10,1,12,18,6,2,3,0,0,2,30,1,1,3,2,3,1,1,4,5,30,2,1,3,1,4,2,3,1,1,6,7,45,3,1,3,1,4,1,6,2,3,1,1,8,9,45,4,1,3,1,4,1,6,1,8,2,3,1,1,10,11,60,5,1,3,1,4,1,6,1,8,1,10,10,3,1,1,12,13,60,6,1,3,1,4,1,6,1,8,1,10,1,12]]
	end

	default("string", "qol-custom-config", config)
end

if mods["quick-adjustable-inserters"] then
	default("bool", "qai-show-throughput-on-inserter", true)
	default("bool", "qai-show-throughput-on-pickup", true)
end

if mods["QuickItemSearch"] then
	default("bool", "qis-fuzzy-search", true)
end

if mods["Rampant"] then
	default("bool", "rampant--safeBuildings-curvedRail", true)
	default("bool", "rampant--safeBuildings-straightRail", true)
	default("bool", "rampant--safeBuildings-bigElectricPole", true)
	default("bool", "rampant--safeBuildings-railSignals", true)
	default("bool", "rampant--safeBuildings-railChainSignals", true)
	default("bool", "rampant--safeBuildings-trainStops", true)
	default("bool", "rampant--safeBuildings-lamps", true)
	default("int", "rampant--attackWaveMaxSize", 150)
	default("bool", "rampant--unitsAffectedByTiles", not mods["Krastorio2"] and not mods["Warmonger"])
	default("bool", "rampant--enableShrinkNestsAndWorms", true)
	default("bool", "rampant--addWallResistanceAcid", true)
	default("bool", "rampant--disallowFriendlyFire", true)
	default("bool", "rampant--newEnemies", false)
	default("bool", "rampant--acidEnemy", false)
	default("bool", "rampant--suicideEnemy", false)
	default("bool", "rampant--fireEnemy", false)
	default("bool", "rampant--electricEnemy", false)
	default("bool", "rampant--nuclearEnemy", false)
	default("bool", "rampant--infernoEnemy", false)
	default("bool", "rampant--trollEnemy", false)
	default("bool", "rampant--laserEnemy", false)
	default("bool", "rampant--energyThiefEnemy", false)
	default("bool", "rampant--poisonEnemy", false)
	default("bool", "rampant--unitSpawnerBreath", true)
	default("bool", "rampant--enableLandfillOnDeath", false)
end

if mods["reverse-factory"] then
	default("int", "rf-delay", 60)
	default("int", "rf-timer", 180)
	default("int", "rf-efficiency", 80)
	default("bool", "rf-vehicles", true)
end

if mods["robot_attrition"] then
	default("string", "robot-attrition-repair", "Repair75")
end

if mods["Rocket-Silo-Construction"] then
	default("bool", "rsc-st-not-removable-site", true)
end

if mods["shield-generators"] then
	default("int", "shield-generators-provider-range-basic", 20)
	default("int", "shield-generators-provider-range-advanced", 40)
	default("int", "shield-generators-provider-range-elite", 80)
	default("int", "shield-generators-provider-range-ultimate", 160)
end

if mods["Shortcuts-ick"] then
	default("int", "grid-radius", 1024)
	default("double", "grid-chunk-line-width", 4)
	default("int", "grid-step", 4)
	default("double", "grid-line-width", 0.75)
	default("bool", "disable-flare", false)
	default("bool", "disable-zoom", false)
	default("bool", "flashlight-toggle", false)
	default("bool", "signal-flare", false)
	default("bool", "big-zoom", false)
	default("bool", "minimap", false)
	default("bool", "night-vision-equipment", false)
	default("string", "spidertron-remote", "disabled")
	default("bool", "spidertron-logistics", false)
	default("bool", "spidertron-logistic-requests", false)
	default("bool", "train-mode-toggle", false)
end

if mods["ShuttleTrainRefresh"] then
	default("bool", "shuttle-train-search-ignore-items", false)
	default("bool", "shuttle-train-smart-manual-destinations", true)
	default("string", "shuttle-train-global-exit-action", "Manual")
	default("string", "shuttle-train-global-depot", "Shuttle")
	default("string", "shuttle-train-global-exclude", "Shuttle")
	default("bool", "shuttle-train-global-ignore-manual-trains", true)
end

if mods["solar-calc"] then
	default("bool", "kaktusbot-sc-show-shortcut-bar-button", false)
end

if mods["space-exploration"] then
	default("int", "se-deep-space-belt-speed-2", 120)
	default("bool", "se-deep-space-belt-blue", false)
	default("bool", "se-deep-space-belt-cyan", false)
	default("bool", "se-deep-space-belt-green", false)
	default("bool", "se-add-icon-labels", true)
end

if mods["StatsGui"] then
	default("bool", "statsgui-single-line", false)
	default("bool", "statsgui-show-playtime-in-days", true)
	default("bool", "statsgui-show-sensor-position", true)
	default("bool", "statsgui-show-sensor-pollution", true)
end
if mods["StatsGui-MovementSpeed"] then
	default("bool", "statsgui-ms-jet-fuel", false)
end

if mods["Toxic_biters"] then
	default("double", "tb-DamageScaler", 0.8)
	default("double", "tb-HealthScaler", 0.9)
end

if mods["trainsaver"] then
	default("double", "ts-transition-speed", 1000)
	default("double", "ts-afk-auto-start", 0)
end

if mods["UltimateResearchQueue"] then
	default("bool", "urq-print-completed-message", false)
end

if mods["UMModulesRework"] then
	default("double", "UMModulesRework-SpeedModule-StartConsumption", 0.5)
	default("double", "UMModulesRework-SpeedModule-ConsumptionMultiplierPerLevel", 1.5)
	if mods["space-exploration"] then
		default("double", "UMModulesRework-SpeedModule-PollutionPenaltyMultiplierPerLevel", 1.5)
	end
	default("double", "UMModulesRework-ProductivityModule-StartConsumption", 0.4)
	default("double", "UMModulesRework-ProductivityModule-ConsumptionMultiplierPerLevel", 1.5)
	default("double", "UMModulesRework-ProductivityModule-SpeedPenaltyMultiplierPerLevel", 1.75)
	default("double", "UMModulesRework-ProductivityModule-PollutionPenaltyMultiplierPerLevel", 1.5)
	default("int", "UMModulesRework-RoundingTo", 1)

	if mods["modules-t4"] then
		for _, m in pairs({"speed", "efficiency", "productivity"}) do
			for _, e in pairs({"consumption", "speed", "productivity", "pollution"}) do
				local n = "t4-modules-" .. m .. "-item-effect-" .. e
				if data.raw["double-setting"][n] ~= nil then
					hide("double", n)
				end
			end
		end
	end
end

if mods["zithorian-extra-storage-tanks"] then
	for _, s in pairs({"scaling", "1x1", "2x2", "3x4", "5x5"}) do
		hide("double", "z-fluid-tank-volume-" .. s)
	end
end
