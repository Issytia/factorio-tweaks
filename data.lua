require("functions")

-- cheaper wires
if settings.startup["issy-cheap-wires"].value then
	for _, wire in pairs({ "red-wire", "green-wire" }) do
		data.raw.recipe[wire].result_count = 10
		mod_ingredient_count(wire, "copper-cable", 10)
	end
end
if mods["PickerTweaks"] and settings.startup["picker-free-circuit-wires"].value then
	local effects = data.raw.technology["circuit-network"].effects
	for _, wire in pairs({ "red-wire", "green-wire" }) do
		data.raw.recipe[wire].enabled = true
		for i = #effects, 1, -1 do
			if effects[i].type == "unlock-recipe" and effects[i].recipe == wire then
				table.remove(effects, i)
			end
		end
	end
end

-- expensive landfill
if settings.startup["issy-expensive-landfill"].value then
	data.raw.recipe["landfill"].energy_required = 20
	mod_ingredient_count("landfill", "stone", 400)

	if mods["bzaluminum"] then
		data.raw.recipe["landfill-silica"].energy_required = 20
		mod_ingredient_count("landfill-silica", "stone", 200)
		mod_ingredient_count("landfill-silica", "silica", 400)
	end

	if mods["Factorio-Tiberium"] then
		data.raw.recipe["tiberium-sludge-to-landfill"].energy_required = 60
		mod_ingredient_count("tiberium-sludge-to-landfill", "tiberium-sludge", 400)
	end

	if mods["Krastorio2"] then
		data.raw.recipe["landfill-2"].energy_required = 20
		mod_ingredient_count("landfill-2", "sand", 1000)
		mod_ingredient_count("landfill-2", "water", 1000)
	end

	if mods["IndustrialRevolution3"] then
		mod_ingredient_count("landfill", "silica", 400)
		mod_ingredient_count("landfill", "gravel", 400)
	end
end

-- add charge virtual signal and set it as default for accumulators
data:extend({
	{
		type = "item-subgroup",
		name = "misc-signals",
		group = "signals",
		order = "z[misc-signals]"
	},
	{
		type = "virtual-signal",
		name = "signal-charge",
		icon = "__base__/graphics/icons/battery-equipment.png",
		icon_size = 64,
		subgroup = "misc-signals",
		order = "z[misc-signals]-a[signal-charge]"
	}
})
data.raw.accumulator["accumulator"].default_output_signal = { type = "virtual", name = "signal-charge" }
if mods["Krastorio2"] then
	data.raw.accumulator["kr-energy-storage"].default_output_signal = { type = "virtual", name = "signal-charge" }
end

-- improve power poles
if settings.startup["issy-power-pole"].value then
	if not mods["omnimatter_energy"] then
		data.raw["electric-pole"]["small-electric-pole"].maximum_wire_distance = 13.5
		data.raw["electric-pole"]["small-electric-pole"].supply_area_distance = 3.5
	else
		data.raw["electric-pole"]["small-electric-pole"].maximum_wire_distance = 8.5
	end
	data.raw["electric-pole"]["medium-electric-pole"].maximum_wire_distance = 19.5
	data.raw["electric-pole"]["medium-electric-pole"].supply_area_distance = 5.5
	data.raw["electric-pole"]["medium-electric-pole"].resistances = { { type = "fire", percent = 75 } }
	data.raw["electric-pole"]["medium-electric-pole"].max_health = 200
	data.raw["electric-pole"]["big-electric-pole"].maximum_wire_distance = 64
	data.raw["electric-pole"]["big-electric-pole"].supply_area_distance = 3
	data.raw["electric-pole"]["big-electric-pole"].resistances = { { type = "fire", percent = 75 } }
	data.raw["electric-pole"]["big-electric-pole"].max_health = 250
	data.raw["electric-pole"]["substation"].maximum_wire_distance = 40.5
	data.raw["electric-pole"]["substation"].supply_area_distance = 16
	data.raw["electric-pole"]["substation"].resistances = { { type = "fire", percent = 50 } }

	if mods["cargo-ships"] then
		data.raw["electric-pole"]["floating-electric-pole"].maximum_wire_distance = 64
		data.raw["electric-pole"]["floating-electric-pole"].resistances = { { type = "fire", percent = 75 } }
		data.raw["electric-pole"]["floating-electric-pole"].max_health = 250
	end

	if mods["IndustrialRevolution3"] then
		for _, type in pairs({ "bronze", "iron" }) do
			data.raw["electric-pole"]["small-" .. type .. "-pole"].maximum_wire_distance = 13.5
			data.raw["electric-pole"]["small-" .. type .. "-pole"].supply_area_distance = 3.5
		end
		data.raw["electric-pole"]["big-wooden-pole"].maximum_wire_distance = 32
		data.raw["electric-pole"]["big-wooden-pole"].supply_area_distance = 3
	end

	if mods["Krastorio2"] then
		data.raw["electric-pole"]["kr-substation-mk2"].maximum_wire_distance = 60.5
		data.raw["electric-pole"]["kr-substation-mk2"].supply_area_distance = 24
		data.raw["electric-pole"]["kr-substation-mk2"].resistances = { { type = "fire", percent = 50 } }
	end

	if mods["omnimatter_energy"] then
		data.raw["electric-pole"]["small-iron-electric-pole"].maximum_wire_distance = 8.5
		data.raw["electric-pole"]["small-iron-electric-pole"].supply_area_distance = 3.5
		data.raw["electric-pole"]["small-omnium-electric-pole"].maximum_wire_distance = 15.5
		data.raw["electric-pole"]["small-omnium-electric-pole"].supply_area_distance = 4.5
	end
end

-- add an electric boiler
if settings.startup["issy-electric-boiler"].value then
	local entity = table.deepcopy(data.raw.boiler["boiler"])
	entity.name = "electric-boiler"
	entity.minable.result = entity.name
	entity.energy_source = {
		type = "electric",
		usage_priority = "secondary-input"
	}

	local item = table.deepcopy(data.raw.item["boiler"])
	item.name = entity.name
	item.order = "b[steam-power]-a[" .. item.name .. "]"
	item.place_result = entity.name

	local recipe = {
		type = "recipe",
		enabled = false,
		energy_required = 5,
		name = entity.name,
		ingredients = { { "boiler", 1 }, { "electronic-circuit", 2 } },
		result = item.name
	}

	data:extend({ entity, item, recipe })

	local effects, pos = data.raw.technology["fluid-handling"].effects
	for i = 1, #effects do
		if effects[i].type == "unlock-recipe" and effects[i].recipe == "pump" then
			pos = i + 1
			break
		end
	end
	if pos == nil then pos = #effects end

	table.insert(effects, pos, { type = "unlock-recipe", recipe = recipe.name })
end

-- add fastforward
data:extend({ {
	type = "custom-input",
	name = "issy-fastforward",
	key_sequence = "CONTROL + TAB",
	consuming = "game-only"
} })

-- add hotkeys to toggle minimap and quickbar
data:extend({ {
	type = "custom-input",
	name = "issy-minimap",
	key_sequence = "CONTROL + SHIFT + M",
	consuming = "game-only"
}, {
	type = "custom-input",
	name = "issy-quickbar",
	key_sequence = "CONTROL + SHIFT + Q",
	consuming = "game-only"
} })

-- increase tile stack sizes and remove all decorations when placing them
if settings.startup["issy-tile-stack"].value then
	for _, item in pairs({
		"concrete", "hazard-concrete", "refined-concrete", "refined-hazard-concrete"
	}) do data.raw.item[item].stack_size = settings.startup["issy-tile-stack"].value end
	if settings.startup["issy-tile-brick"].value then
		data.raw.item["stone-brick"].stack_size = settings.startup["issy-tile-stack"].value
	end
	if settings.startup["issy-tile-landfill"].value then
		data.raw.item["landfill"].stack_size = settings.startup["issy-tile-stack"].value
	end
	if mods["IndustrialRevolution"] then
		data.raw.item["stone-covering"].stack_size = settings.startup["issy-tile-stack"].value
		data.raw.item["carbon-covering"].stack_size = settings.startup["issy-tile-stack"].value
	end
	if mods["IndustrialRevolution3"] then
		data.raw.item["tarmac"].stack_size = settings.startup["issy-tile-stack"].value
	end
	if mods["Krastorio2"] then
		data.raw.item["kr-black-reinforced-plate"].stack_size = settings.startup["issy-tile-stack"].value
		data.raw.item["kr-white-reinforced-plate"].stack_size = settings.startup["issy-tile-stack"].value
		data.raw.item["kr-creep"].stack_size = settings.startup["issy-tile-stack"].value
	end
	if mods["Transport_Drones"] then
		data.raw.item["road"].stack_size = settings.startup["issy-tile-stack"].value
		data.raw.item["fast-road"].stack_size = settings.startup["issy-tile-stack"].value
	end
end
if settings.startup["issy-tile-clear"].value then
	for _, tile in pairs({
		"stone-path", "concrete", "hazard-concrete-left", "hazard-concrete-right", "refined-concrete",
		"refined-hazard-concrete-left", "refined-hazard-concrete-right"
	}) do data.raw.tile[tile].decorative_removal_probability = 1 end
	if mods["IndustrialRevolution"] then
		data.raw.tile["stone-covering"].decorative_removal_probability = 1
		data.raw.tile["carbon-covering"].decorative_removal_probability = 1
	end
	if mods["Krastorio2"] then
		data.raw.tile["kr-black-reinforced-plate"].decorative_removal_probability = 1
		data.raw.tile["kr-white-reinforced-plate"].decorative_removal_probability = 1
	end
	if mods["Transport_Drones"] then
		data.raw.tile["transport-drone-road"].decorative_removal_probability = 1
		data.raw.tile["transport-drone-road-better"].decorative_removal_probability = 1
	end
	if mods["omnimatter"] then
		data.raw.tile["omnite-brick"].decorative_removal_probability = 1
	end
end
if settings.startup["issy-explosives-stack"].value then
	data.raw.capsule["cliff-explosives"].stack_size = settings.startup["issy-explosives-stack"].value
	if mods["Explosive Excavation"] then
		data.raw.item["blasting-charge"].stack_size = settings.startup["issy-explosives-stack"].value
	end
	if mods["IndustrialRevolution3"] then
		data.raw.item["waterfill-explosive"].stack_size = settings.startup["issy-explosives-stack"].value
	end
end

-- reduce inventory sizes
if settings.startup["issy-shrink-inventories"].value then
	local mult = 2.5

	data.raw.car["car"].inventory_size = 20
	data.raw.car["tank"].inventory_size = 10
	data.raw["cargo-wagon"]["cargo-wagon"].inventory_size = 20
	data.raw["fluid-wagon"]["fluid-wagon"].capacity = 10000
	data.raw.container["wooden-chest"].inventory_size = 4
	data.raw.container["iron-chest"].inventory_size = 8
	data.raw.container["steel-chest"].inventory_size = 16
	data.raw["logistic-container"]["logistic-chest-passive-provider"].inventory_size = 16
	data.raw["logistic-container"]["logistic-chest-active-provider"].inventory_size = 16
	data.raw["logistic-container"]["logistic-chest-storage"].inventory_size = 16
	data.raw["logistic-container"]["logistic-chest-buffer"].inventory_size = 16
	data.raw["logistic-container"]["logistic-chest-requester"].inventory_size = 24
	data.raw["storage-tank"]["storage-tank"].fluid_box.base_area = 100

	if mods["aai-containers"] then
		local strongbox = data.raw.container["steel-chest"].inventory_size
				* mult * 2
		local strongbox_mid = strongbox * mult
		local storehouse = strongbox_mid * mult
		local storehouse_mid = storehouse * (mult / 1.5625) -- kind of arbitrary, but in between
		local warehouse = storehouse * mult
		data.raw.container["aai-strongbox"].inventory_size = strongbox
		for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
			data.raw["logistic-container"]["aai-strongbox-" .. type].inventory_size = strongbox
		end
		if mods["3x3-container-for-aai-containers"] then
			data.raw.container["aai-strongbox-3x3"].inventory_size = strongbox_mid
			for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
				data.raw["logistic-container"]["aai-strongbox-3x3-" .. type].inventory_size = strongbox_mid
			end
		end
		if mods["5x5-container-for-aai-containers"] then
			data.raw.container["aai-storehouse-5x5"].inventory_size = storehouse_mid
			for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
				data.raw["logistic-container"]["aai-storehouse-5x5-" .. type].inventory_size = storehouse_mid
			end
		end
		data.raw.container["aai-storehouse"].inventory_size = storehouse
		for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
			data.raw["logistic-container"]["aai-storehouse-" .. type].inventory_size = storehouse
		end
		data.raw.container["aai-warehouse"].inventory_size = warehouse
		for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
			data.raw["logistic-container"]["aai-warehouse-" .. type].inventory_size = warehouse
		end
	end

	if mods["aai-programmable-vehicles"] then
		data.raw.car["tank"].inventory_size = 10
	end
	if mods["aai-vehicles-chaingunner"] then
		data.raw.car["vehicle-chaingunner"].inventory_size = 10
	end
	if mods["aai-vehicles-flame-tank"] then
		data.raw.car["vehicle-flame-tank"].inventory_size = 10
	end
	if mods["aai-vehicles-flame-tumbler"] then
		data.raw.car["vehicle-flame-tumbler"].inventory_size = 10
	end
	if mods["aai-vehicles-hauler"] then
		data.raw.car["vehicle-hauler"].inventory_size = 80
	end
	if mods["aai-vehicles-ironclad"] then
		data.raw.car["ironclad"].inventory_size = 20
		if mods["ironclad-gunboat-and-mortar-turret"] then
			data.raw.car["ironclad-gunboat"].inventory_size = 20
		end
	end
	if mods["aai-vehicles-laser-tank"] then
		data.raw.car["vehicle-laser-tank"].inventory_size = 10
	end
	if mods["aai-vehicles-miner"] then
		for i, name in pairs({ "", "-mk2", "-mk3", "-mk4", "-mk5" }) do
			data.raw.car["vehicle-miner" .. name].inventory_size = 10 + 5 * (i - 1)
		end
	end
	if mods["aai-vehicles-warden"] then
		data.raw.car["vehicle-warden"].inventory_size = 40
	end

	if mods["bzlead"] and data.raw.container["lead-chest"] then
		data.raw.container["lead-chest"].inventory_size = 6
	end

	if mods["cargo-ships"] then
		data.raw["cargo-wagon"]["cargo_ship"].inventory_size = 200
		data.raw["fluid-wagon"]["oil_tanker"].capacity = 100000
	end

	if mods["extra-storage-tank"] then
		data.raw["storage-tank"]["steel-storage-tank"].fluid_box.base_area = 500
	end
	if mods["extra-storage-tank-minibuffer"] then
		data.raw["storage-tank"]["minibuffer"].fluid_box.base_area = 10
	end

	if mods["Hovercrafts"] then
		data.raw.car["hcraft-entity"].inventory_size = 20
		data.raw.car["mcraft-entity"].inventory_size = 10
	end

	if mods["IndustrialRevolution"] or mods["IndustrialRevolution3"] then
		data.raw.car["monowheel"].inventory_size = 10
		data.raw["cargo-wagon"]["cargo-wagon"].inventory_size = 20
		data.raw["storage-tank"]["small-tank"].fluid_box.base_area = 10
		data.raw["storage-tank"]["small-tank-steam"].fluid_box.base_area = 10
		data.raw.container["wooden-chest"].inventory_size = 2
		if mods["IndustrialRevolution3"] then
			data.raw.container["tin-pallet"].inventory_size = 2
			--data.raw.container["bronze-pallet"].inventory_size = 3
		end
		data.raw.container["tin-chest"].inventory_size = 3
		data.raw.container["bronze-chest"].inventory_size = 4
		data.raw.container["iron-chest"].inventory_size = 8
		data.raw.container["steel-chest"].inventory_size = 16
		for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer" }) do
			data.raw["logistic-container"]["logistic-chest-" .. type].inventory_size = 16
		end
		data.raw["logistic-container"]["logistic-chest-requester"].inventory_size = 24
	end

	if mods["Krastorio2"] then
		data.raw.car["kr-advanced-tank"].inventory_size = 10
		if settings.startup["kr-containers"].value then
			local medium = data.raw.container["steel-chest"].inventory_size
					* mult * 2
			local big = medium * mult * 2
			data.raw.container["kr-medium-container"].inventory_size = medium
			data.raw.container["kr-big-container"].inventory_size = big
			for _, type in pairs({ "passive-provider", "active-provider", "storage", "buffer", "requester" }) do
				data.raw["logistic-container"]["kr-medium-" .. type .. "-container"].inventory_size = medium
				data.raw["logistic-container"]["kr-big-" .. type .. "-container"].inventory_size = big
				if type == "requester" or type == "buffer" then
					data.raw["logistic-container"]["kr-medium-" .. type .. "-container"].max_logistic_slots = nil
					data.raw["logistic-container"]["kr-big-" .. type .. "-container"].max_logistic_slots = nil
				end
			end
		end
		data.raw["storage-tank"]["kr-fluid-storage-1"].fluid_box.base_area = 200
		data.raw["storage-tank"]["kr-fluid-storage-2"].fluid_box.base_area = 1000
	end

	if mods["Mini_Trains"] then
		data.raw["cargo-wagon"]["mini-cargo-wagon"].inventory_size = 10
		data.raw["fluid-wagon"]["mini-fluid-wagon"].capacity = 5000
	end

	if mods["RenaiTransportation"] then
		data.raw.container["OpenContainer"].inventory_size = 8
	end

	if mods["TeslaTank"] then
		data.raw.car["Teslatank"].inventory_size = 10
	end

	if mods["Transport_Drones"] then
		local boxes = data.raw.furnace["fluid-depot"].fluid_boxes
		for i = 1, #boxes do
			if boxes[i].production_type == "input" then boxes[i].base_area = 100 end
		end
	end

	if mods["zithorian-extra-storage-tanks"] then
		data.raw["storage-tank"]["fluid-tank-1x1"].fluid_box.base_area = 10
		data.raw["storage-tank"]["fluid-tank-1x1"].localised_description = ""
		data.raw["storage-tank"]["fluid-tank-2x2"].fluid_box.base_area = 40
		data.raw["storage-tank"]["fluid-tank-2x2"].localised_description = ""
		data.raw["storage-tank"]["fluid-tank-3x4"].fluid_box.base_area = 360
		data.raw["storage-tank"]["fluid-tank-3x4"].localised_description = ""
		data.raw["storage-tank"]["fluid-tank-5x5"].fluid_box.base_area = 800
		data.raw["storage-tank"]["fluid-tank-5x5"].localised_description = ""
	end
end

-- adjust solar lamp recipes
if mods["Solar-Lamp"] then
	if mods["bzsilicon"] and data.raw.item["solar-cell"] then
		mod_ingredient("solar-lamp-recipe", "solar-panel", "solar-cell", 2)
		mod_ingredient("large-solar-lamp-recipe", "solar-panel", "solar-cell", 4)
	end
	if mods["DeadlockLargerLamp"] then
		mod_ingredient("large-solar-lamp-recipe", "small-lamp", "deadlock-large-lamp", 1)
	end
end

-- remove research speed techs
if settings.startup["issy-no-research-speed"].value then
	for i = 1, 6 do
		data.raw.technology["research-speed-" .. i] = nil
	end
end

-- fix infinite repair pack prerequisites
if mods["Infinite_Repair_Pack_Revived"] then
	table.insert(data.raw.technology["infinite-repair-pack"].prerequisites, "advanced-electronics")
end

-- add fish-farm category, and add recipes for more-fish
if mods["Nihilistzsche_FishFarm"] then
	data:extend({
		{
			type = "recipe-category",
			name = "fish-farm"
		}
	})

	data.raw.recipe["raw-fish"].category = "fish-farm"
	data.raw["assembling-machine"]["fish-farm"].crafting_categories = { "fish-farm" }

	if mods["more-fish"] then
		data.raw.recipe["raw-fish"].hidden = false
		data.raw.recipe["raw-fish"].hide_from_player_crafting = true
		data.raw["assembling-machine"]["fish-farm"].fixed_recipe = nil

		for _, name in pairs({ "salmon", "cod", "pufferfish", "clownfish" }) do
			data.raw.capsule["raw-" .. name].subgroup = data.raw.capsule["raw-fish"].subgroup

			local recipe = table.deepcopy(data.raw.recipe["raw-fish"])
			recipe.name = "raw-" .. name
			recipe.result = recipe.name
			data:extend({ recipe })
		end
	end
end

-- add fixed pipes
local base_pipe = data.raw.pipe["pipe"]
local empty_sprite = { filename = "__core__/graphics/empty.png", size = 1, frame_count = 1 }
local connections = {
	["elbow"] = {
		{ position = { 1, 0 } },
		{ position = { 0, 1 } }
	},
	["junction"] = {
		{ position = { 1, 0 } },
		{ position = { 0, 1 } },
		{ position = { -1, 0 } }
	},
	["straight"] = {
		{ position = { 0, -1 } },
		{ position = { 0, 1 } }
	}
}
local pictures = {
	["elbow"] = {
		north = pipepictures().corner_down_right,
		east = pipepictures().corner_down_left,
		south = pipepictures().corner_up_left,
		west = pipepictures().corner_up_right
	},
	["junction"] = {
		north = pipepictures().t_down,
		east = pipepictures().t_left,
		south = pipepictures().t_up,
		west = pipepictures().t_right
	},
	["straight"] = {
		north = pipepictures().straight_vertical,
		east = pipepictures().straight_horizontal,
		south = pipepictures().straight_vertical,
		west = pipepictures().straight_horizontal
	}
}

for _, name in pairs({ "elbow", "junction", "straight" }) do
	local pipe = table.deepcopy(data.raw["storage-tank"]["storage-tank"])
	pipe.name = "pipe-" .. name
	pipe.icon = "__issy-tweaks__/graphics/" .. pipe.name .. ".png"
	pipe.icon_size = 64
	pipe.icon_mipmaps = nil
	pipe.minable = base_pipe.minable
	pipe.corpse = base_pipe.corpse
	pipe.max_health = base_pipe.max_health
	pipe.resistances = base_pipe.resistances
	pipe.fast_replaceable_group = base_pipe.fast_replaceable_group
	pipe.placeable_by = { item = "pipe", count = 1 }
	pipe.collision_box = base_pipe.collision_box
	pipe.selection_box = base_pipe.selection_box
	pipe.dying_explosion = base_pipe.dying_explosion
	pipe.friendly_map_color = { 69, 130, 165 }
	pipe.water_reflection = nil
	pipe.fluid_box = {
		base_area = base_pipe.fluid_box.base_area,
		height = base_pipe.fluid_box.height,
		pipe_covers = pipecoverspictures(),
		pipe_connections = connections[name]
	}
	pipe.two_direction_only = false
	pipe.pictures = {
		picture = pictures[name],
		gas_flow = empty_sprite,
		fluid_background = empty_sprite,
		window_background = empty_sprite,
		flow_sprite = empty_sprite
	}
	pipe.circuit_wire_max_distance = 0
	pipe.working_sound = nil

	local item = {
		type = "item",
		name = pipe.name,
		icon = pipe.icon,
		icon_size = pipe.icon_size,
		subgroup = data.raw.item["pipe"].subgroup,
		order = "b[pipe]-a[" .. pipe.name .. "]",
		place_result = pipe.name,
		stack_size = 20
	}

	local recipe = {
		type = "recipe",
		name = pipe.name,
		energy_required = 0.01,
		ingredients = { { "pipe", 1 } },
		result = pipe.name
	}

	data:extend({ pipe, item, recipe })
end

if mods["Krastorio2"] then
	require("__Krastorio2__.prototypes.entities.buildings.pipe-covers.steel-pipecovers")

	base_pipe = data.raw.pipe["kr-steel-pipe"]
	local icon_size = {
		["elbow"] = 80,
		["junction"] = 90,
		["straight"] = 107
	}
	pictures = {
		["elbow"] = {
			north = base_pipe.pictures.corner_down_right,
			east = base_pipe.pictures.corner_down_left,
			south = base_pipe.pictures.corner_up_left,
			west = base_pipe.pictures.corner_up_right
		},
		["junction"] = {
			north = base_pipe.pictures.t_down,
			east = base_pipe.pictures.t_left,
			south = base_pipe.pictures.t_up,
			west = base_pipe.pictures.t_right
		},
		["straight"] = {
			north = base_pipe.pictures.straight_vertical,
			east = base_pipe.pictures.straight_horizontal,
			south = base_pipe.pictures.straight_vertical,
			west = base_pipe.pictures.straight_horizontal
		}
	}

	for _, name in pairs({ "elbow", "junction", "straight" }) do
		local pipe = table.deepcopy(data.raw["storage-tank"]["storage-tank"])
		pipe.name = "kr-steel-pipe-" .. name
		pipe.icon = "__issy-tweaks__/graphics/kr-steel-pipe-" .. name .. ".png"
		pipe.icon_size = icon_size[name]
		pipe.icon_mipmaps = nil
		pipe.minable = base_pipe.minable
		pipe.corpse = base_pipe.corpse
		pipe.max_health = base_pipe.max_health
		pipe.resistances = base_pipe.resistances
		pipe.fast_replaceable_group = base_pipe.fast_replaceable_group
		pipe.placeable_by = { item = "kr-steel-pipe", count = 1 }
		pipe.collision_box = base_pipe.collision_box
		pipe.selection_box = base_pipe.selection_box
		pipe.dying_explosion = base_pipe.dying_explosion
		pipe.friendly_map_color = { 69, 130, 165 }
		pipe.water_reflection = nil
		pipe.fluid_box = {
			base_area = base_pipe.fluid_box.base_area,
			height = base_pipe.fluid_box.height,
			pipe_covers = steel_pipecoverspictures,
			pipe_connections = connections[name]
		}
		pipe.two_direction_only = false
		pipe.pictures = {
			picture = pictures[name],
			gas_flow = empty_sprite,
			fluid_background = empty_sprite,
			window_background = empty_sprite,
			flow_sprite = empty_sprite
		}
		pipe.circuit_wire_max_distance = 0
		pipe.working_sound = nil

		local item = {
			type = "item",
			name = pipe.name,
			icon = pipe.icon,
			icon_size = pipe.icon_size,
			subgroup = data.raw.item["kr-steel-pipe"].subgroup,
			order = "b[pipe]-a2[steel-pipe-" .. name .. "]",
			place_result = pipe.name,
			stack_size = 20
		}

		local recipe = {
			type = "recipe",
			name = pipe.name,
			energy_required = 0.01,
			ingredients = { { "kr-steel-pipe", 1 } },
			result = pipe.name
		}

		data:extend({ pipe, item, recipe })
	end
end

-- small nightvision and discharge equipment
if settings.startup["issy-small-nightvision"].value then
	if data.raw["night-vision-equipment"]["night-vision-equipment"] ~= nil then
		data.raw["night-vision-equipment"]["night-vision-equipment"].shape.width = 1
		data.raw["night-vision-equipment"]["night-vision-equipment"].shape.height = 1
	end

	if data.raw["active-defense-equipment"]["discharge-defense-equipment"] ~= nil then
		data.raw["active-defense-equipment"]["discharge-defense-equipment"].shape.width = 1
		data.raw["active-defense-equipment"]["discharge-defense-equipment"].shape.height = 1
	end
end

-- add rail/death world with rich resources map gen preset
data.raw["map-gen-presets"]["default"]["custom"] = {
	order = "a",
	basic_settings = {
		property_expression_names = {},
		autoplace_controls = {
			["iron-ore"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["copper-ore"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["stone"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["coal"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["uranium-ore"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["crude-oil"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["trees"] = { frequency = 0.3333333333, size = 3, richness = 3 },
			["enemy-base"] = { frequency = 2, size = 2 }
		},
		terrain_segmentation = 2,
		water = 1.5,
		starting_area = 1.5
	},
	advanced_settings = {
		enemy_evolution = {
			time_factor = 0.00002,
			pollution_factor = 0.0000012
		},
		enemy_expansion = {
			enabled = false
		},
		pollution = {
			ageing = 0.5,
			enemy_attack_pollution_consumption_modifier = 0.5
		}
	}
}

-- add mineral water to starting area
if mods["Krastorio2"] then
	data.raw.resource["mineral-water"].autoplace = require("resource-autoplace").resource_autoplace_settings({
		name = "mineral-water",
		order = "c",
		base_density = 2,
		base_spots_per_km2 = 0.5,
		random_probability = 1 / 50,
		random_spot_size_minimum = 1,
		random_spot_size_maximum = 1,
		additional_richness = 50000,
		has_starting_area_placement = true,
		regular_rq_factor_multiplier = 1,
	})
end

-- make hazard concrete less tedious
if settings.startup["issy-hazard-concrete"].value then
	for _, tile in pairs({ "", "refined-" }) do
		for _, dir in pairs({ "left", "right" }) do
			data.raw.tile[tile .. "hazard-concrete-" .. dir].minable.result = tile .. "concrete"
			data.raw.tile[tile .. "hazard-concrete-" .. dir].placeable_by = {
				{ item = tile .. "hazard-concrete", count = 1 },
				{ item = tile .. "concrete",        count = 1 }
			}
		end
	end
end

-- earlier and independent coal liquefaction
if settings.startup["issy-coal-liquefaction"].value then
	remove_prereq("coal-liquefaction", "production-science-pack")
	mod_ingredient_count_tech("coal-liquefaction", "production-science-pack", 0)

	data:extend({
		{
			type = "recipe",
			name = "coal-compaction",
			icon = "__issy-tweaks__/graphics/recipe-compaction.png",
			icon_size = 64,
			ingredients = {
				{type = "item", name = "coal", amount = 10},
				{type = "fluid", name = "steam", amount = 50, fluidbox_index = 2},
			},
			results = {
				{type = "fluid", name = "heavy-oil", amount = 0.25, fluidbox_index = 1},
				{type = "fluid", name = "petroleum-gas", amount = 10, fluidbox_index = 3},
			},
			category = "oil-processing",
			subgroup = "fluid-recipes",
			energy_required = 5
		},
		{
			type = "technology",
			name = "coal-compaction",
			icon = "__issy-tweaks__/graphics/tech-compaction.png",
			icon_size = 128,
			order = "a",
			prerequisites = {"advanced-material-processing", "oil-processing"},
			effects = {{type = "unlock-recipe", recipe = "coal-compaction"}},
			unit = {
					count = 100,
					ingredients = {
						{"automation-science-pack", 1},
						{"logistic-science-pack", 1}
					},
					time = 30
			},
		}
	})
end

-- cheaper logistics
if settings.startup["issy-cheaper-logistics"].value then
	mod_ingredient_count("underground-belt", "iron-plate", 4)
	mod_ingredient_counts("splitter", { { "electronic-circuit", 2 }, { "iron-plate", 2 } })
	mod_ingredient_count("fast-transport-belt", "iron-gear-wheel", 3)
	mod_ingredient_count("fast-underground-belt", "iron-gear-wheel", 12)
	mod_ingredient_counts("fast-splitter", { { "iron-gear-wheel", 5 }, { "electronic-circuit", 5 } })
	mod_ingredient_count("express-transport-belt", "iron-gear-wheel", 6)
	mod_ingredient_counts("express-underground-belt", { { "iron-gear-wheel", 24 }, { "lubricant", 0 } })
	data.raw.recipe["express-underground-belt"].category = nil
	mod_ingredient_counts("express-splitter", { { "advanced-circuit", 5 }, { "lubricant", 0 } })
	data.raw.recipe["express-splitter"].category = nil
end
